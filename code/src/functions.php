<?php
require_once(__DIR__ . '/vendor/autoload.php');
$timber = new Timber\Timber();

require 'theme_code/functions/load_acf.php';
require 'theme_code/functions/scripts.php';
require 'theme_code/classes/CPT.php';
require 'theme_code/classes/custom_post_types/doctors.php';
require 'theme_code/classes/custom_post_types/testimonials.php';
require 'theme_code/classes/custom_post_types/locations.php';
require 'theme_code/classes/custom_post_types/services.php';
require 'theme_code/classes/JDWebsite.php';
require 'theme_code/classes/widgets/button-link.php';
require 'theme_code/classes/widgets/sidebar-navigation.php';
require 'theme_code/classes/widgets/the-latest.php';

require 'theme_code/functions/menus.php';
require 'theme_code/functions/options.php';
require 'theme_code/functions/shortcodes.php';
require 'theme_code/functions/widgets_enable.php';

function get_id_by_slug(string $page_slug)
{
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}
