<?php
namespace doctors;
use Timber\Menu as Menu;
use Timber\Post as Post;
use Timber\Timber as Timber;
use Timber\PostQuery;

class JDWebsite{
    protected   $context;
    public      $debug = false,
                $cache = false; // 600

    public function __construct(){
        $this->context = Timber::get_context();
        $this->add_context();
        $this->render_view();
    }

    /**
     * Echoes out $this->context in a semi-readable format
     */
    public function debug(){
        echo '<pre>' . print_r($this->context, true) . '</pre>';
    }

    /**
     * @return bool
     * Checks whether or not a view file exists for the requested page based off of its slug.
     */
    protected function view_file_exists(){
        return file_exists(get_template_directory() . '/views/pages/' . $this->context['post']->slug . '.twig');
    }

    /**
     * @return bool
     * Checks if this is a post-related page (archive, blog page, etc.)
     */
    protected function is_posts(){
        if(is_home() || is_post_type_archive('post')){
            return true;
        } else{
            return false;
        }
    }

    protected function get_footer_gradient_styles(array $footer_image_options){
        $styles = sprintf('background-image: linear-gradient(to %s, %s, %s %s, %s %s, %s);',
            $footer_image_options['gradient_direction'],
            $footer_image_options['gradient_colors'][0]['color'],
            $footer_image_options['gradient_colors'][1]['color'],
            $footer_image_options['gradient_colors'][1]['starting_point'] . '%',
            $footer_image_options['gradient_colors'][2]['color'],
            $footer_image_options['gradient_colors'][2]['starting_point'] . '%',
            $footer_image_options['gradient_colors'][3]['color']
            );

        return $styles;
    }

    /**
     * Adds Misc. context
     */
    protected function add_context(){
        // Add navigational menus
        $this->context['main_navigation']  = new Menu('main-navigation');
        $this->context['sidebar_dropdown_navigation']  = new Menu('sidebar-dropdown-navigation');
        $this->context['small_print_navigation']  = new Menu('small-print-navigation');

        // Add footer menus
        for($i = 1; $i <= 4; $i++){
            $this->context['footer_navigation_' . $i] = new Menu('footer-navigation-' . $i);
        }

        // Add current post's content
        $this->context['post'] = new Post();

        // Add all site options
        $this->context['site_options'] = get_fields('options');
        $this->context['footer_image_styles'] = $this->get_footer_gradient_styles(get_field('footer_image_settings', 'options'));

        // Include posts if necessary
        if($this->is_posts()){
            $this->context['posts'] = new PostQuery();
        }

        // Do things specific to the frontpage.
        if(is_front_page()){
            $this->context['is_frontpage'] = true;
        }

        $this->context['is_a_post'] = is_singular('post');

        // Widgets
        $this->context['sidebar_widgets'] = Timber::get_widgets('doctors_sidebar');

        // Footer Testimonial
        $this->context['footer_testimonials'] = Timber::get_posts([
            'post_type' => 'testimonials',
            'orderby' => 'rand',
            'posts_per_page' => 1
        ]);

        if(is_page('testimonials')){
            $this->context['testimonials'] = Timber::get_posts([
                'post_type' => 'testimonials',
                'posts_per_page' => -1
            ]);
        }


        if(is_page('locations')){
            $this->context['locations'] = Timber::get_posts([
                'post_type' => 'locations',
                'posts_per_page' => -1
            ]);
        }

        if(is_page('services')){
            $this->context['services'] = Timber::get_posts([
                'post_type' => 'services',
                'posts_per_page' => -1
            ]);
        }

        if($this->debug){
            $this->debug();
        }
    }

    /**
     * Figures out which view file is needed and renders it.
     */
    public function render_view(){
        if($this->view_file_exists()){
            $file = 'pages/' . $this->context['post']->slug . '.twig';
        } elseif(is_singular('doctors')){
            $file = 'pages/doctors-single.twig';
        } elseif(is_front_page()){
            $file = 'pages/home.twig';
        } elseif($this->is_posts()){
            $file = 'archive.twig';
        } elseif(is_404()){
            $file = '404.twig';
        } else{
            $file = 'single.twig';
        }

        Timber::render($file, $this->context, $this->cache);
    }
}
