<?php

namespace doctors;

$doctors = new customPostType();
$doctors->setArg('id', 'doctors');
$doctors->setArg('name_singular', 'Doctor');
$doctors->setArg('name_plural', 'Doctors');
$doctors->setArg('menu_icon', 'dashicons-groups');
$doctors->setArg('has_archive', false);
$doctors->setArg('rewrite', array(
    'slug' => 'doctors',
    'with_front' => true,
    'pages' => true,
    'feeds' => true
));
$doctors->create();