<?php

namespace doctors;

$locations = new customPostType();
$locations->setArg('id', 'locations');
$locations->setArg('name_singular', 'Location');
$locations->setArg('name_plural', 'Locations');
$locations->setArg('menu_icon', 'dashicons-location');
$locations->setArg('has_archive', false);
$locations->setArg('rewrite', [
    'slug' => 'locations',
    'with_front' => true,
    'pages' => true,
    'feeds' => true
]);
$locations->setArg('supports', [
    'title',
    'editor'
]);
$locations->create();