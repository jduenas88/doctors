<?php

namespace doctors;

$services = new customPostType();
$services->setArg('id', 'services');
$services->setArg('name_singular', 'Service');
$services->setArg('name_plural', 'Services');
$services->setArg('menu_icon', 'dashicons-paperclip');
$services->setArg('has_archive', false);
$services->setArg('rewrite', [
    'slug' => 'services',
    'with_front' => true,
    'pages' => true,
    'feeds' => true
]);
$services->setArg('supports', [
    'title',
    'editor'
]);
$services->create();