<?php

namespace doctors;

$testimonials = new customPostType();
$testimonials->setArg('id', 'testimonials');
$testimonials->setArg('name_singular', 'Testimonial');
$testimonials->setArg('name_plural', 'Testimonials');
$testimonials->setArg('menu_icon', 'dashicons-format-quote');
$testimonials->setArg('rewrite', [
    'slug' => 'testimonials',
    'with_front' => true,
    'pages' => true,
    'feeds' => true
]);
$testimonials->setArg('supports', [
    'title',
    'editor'
]);
$testimonials->create();