<?php

class doctors_button_link_widget extends WP_Widget
{
    public function __construct()
    {
        $widget_ops = [
            'url' => get_site_url()
        ];

        parent::__construct('button_link_widget', 'Button Link', $widget_ops);
    }

    public function widget($args, $instance)
    {
        $widget_id = 'widget_' . $args['widget_id'];

        Timber::render('partials/widgets/button-link.twig', [
            'args' => $args,
            'instance' => $instance,
            'button_link' => get_field('button_link', $widget_id),
            'button_text' => get_field('button_text', $widget_id)
        ]);
    }

    public function form($instance)
    {

    }

    public function update($new_instance, $old_instance)
    {

    }
}

add_action('widgets_init', function(){
   register_widget('doctors_button_link_widget');
});