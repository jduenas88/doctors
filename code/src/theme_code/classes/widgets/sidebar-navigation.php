<?php

class doctors_sidebar_navigation_widget extends WP_Widget
{
    public function __construct()
    {
        $widget_ops = [
            'url' => get_site_url()
        ];

        parent::__construct('sidebar_navigation_widget', 'Sidebar Navigation', $widget_ops);
    }

    public function widget($args, $instance)
    {
        $widget_id = 'widget_' . $args['widget_id'];

        Timber::render('partials/widgets/sidebar-navigation.twig', [
            'args' => $args,
            'instance' => $instance,
            'menu_items' => get_field('menu_items', $widget_id),
            'title' => get_field('title', $widget_id)
        ]);
    }

    public function form($instance)
    {

    }

    public function update($new_instance, $old_instance)
    {

    }
}

add_action('widgets_init', function(){
   register_widget('doctors_sidebar_navigation_widget');
});