<?php

class doctors_the_latest_widget extends WP_Widget
{
    public function __construct()
    {
        $widget_ops = [
            'url' => get_site_url()
        ];

        parent::__construct('the_latest_widget', 'The Latest', $widget_ops);
    }

    public function widget($args, $instance)
    {
        $widget_id = 'widget_' . $args['widget_id'];
        $posts = Timber::get_posts([
            'posts_per_page' => get_field('post_count', $widget_id)
        ]);


        Timber::render('partials/widgets/the-latest.twig', [
            'args' => $args,
            'instance' => $instance,
            'title' => get_field('title', $widget_id),
            'posts' => $posts
        ]);
    }

    public function form($instance)
    {

    }

    public function update($new_instance, $old_instance)
    {

    }
}

add_action('widgets_init', function(){
   register_widget('doctors_the_latest_widget');
});