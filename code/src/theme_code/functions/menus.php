<?php

function doctors_register_navigation()
{
    register_nav_menus(
        array(
            'main-navigation'  => __('Main Navigation'),
            'sidebar-dropdown-navigation' => __('Sidebar Dropdown Navigation'),
            'footer-navigation-1' => __('Footer Navigation 1'),
            'footer-navigation-2' => __('Footer Navigation 2'),
            'footer-navigation-3' => __('Footer Navigation 3'),
            'footer-navigation-4' => __('Footer Navigation 4'),
            'small-print-navigation' => __('Small Print Navigation'),
        ));
}

add_action('init', 'doctors_register_navigation');
