<?php

function doctors_scripts(){
    wp_enqueue_style('default-styles', get_stylesheet_uri());
    wp_enqueue_script('doctors_all', get_stylesheet_directory_uri() . '/all.js', array('jquery'), false, true);
}

add_action('wp_enqueue_scripts', 'doctors_scripts');
