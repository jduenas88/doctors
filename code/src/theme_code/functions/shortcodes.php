<?php

function doctors_doctor_grid_shortcode($atts)
{
    $doctors = Timber::get_posts([
        'posts_per_page' => -1,
        'post_type' => 'doctors'
    ]);

    return Timber::compile('partials/shortcodes/doctor-grid.twig', ['doctors' => $doctors]);
}

add_shortcode('doctors-grid', 'doctors_doctor_grid_shortcode');