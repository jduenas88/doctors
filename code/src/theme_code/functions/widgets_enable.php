<?php

function doctors_widgets_init()
{
    register_sidebar([
        'name' => 'Sidebar',
        'id' => 'doctors_sidebar',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ]);
}

add_action('widgets_init', 'doctors_widgets_init');